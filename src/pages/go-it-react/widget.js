import { useState } from "react";
import Notification from "../../components/goit/widget/notification";
import FeedbackOptions from "../../components/goit/widget/feedbackoptions";
import Statistics from "../../components/goit/widget/statistics";
import Section from "../../components/goit/widget/section";

function Widget() {
  const [state, setState] = useState({
    good: 0,
    neutral: 0,
    bad: 0,
  });

  function countTotalFeedback() {
    return state.good + state.neutral + state.bad;
  }

  function countPositiveFeedbackPercentage() {
    return (
      Math.floor(
        (state.good / (state.good + state.neutral + state.bad)) * 100
      ) + "%"
    );
  }

  return (
    <>
      <Section title="Please leave feedback" />
      <FeedbackOptions options={state} onLeaveFeedback={setState} />
      <Section title="Statistic" />
      {countTotalFeedback() ?
        <Statistics
          good={state.good}
          neutral={state.neutral}
          bad={state.bad}
          total={countTotalFeedback()}
          positivePercentage={countPositiveFeedbackPercentage()}
        ></Statistics>
        :
        <Notification message="There is no feedback" />
      }
    </>
  );
}

export default Widget;
