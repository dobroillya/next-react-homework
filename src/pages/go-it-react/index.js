import styled from "styled-components";
import LinksHomework from "../../components/linksHomework";

const exercises = [
  {
    id: "exgoit1",
    name: "Віджет відгуків",
    hrefGitLab: "",
    href: "go-it-react/widget",
    level: "beginner",
  },
  {
    id: "exgoit2",
    name: "Профіль соціальної мережі",
    hrefGitLab: "",
    href: "go-it-react/profile",
    level: "beginner",
  },
  {
    id: "exgoit3",
    name: "Список друзів",
    hrefGitLab: "",
    href: "go-it-react/list-friends",
    level: "beginner",
  },
  {
    id: "exgoit4",
    name: "Книга контактів",
    hrefGitLab: "",
    href: "go-it-react/contacts-book",
    level: "beginner",
  },
];

function GoitReact() {
  return (
    <>
      <Wrapper>
        <Title>Goitacademy React homework</Title>
      </Wrapper>
      <LinksHomework list={exercises} />
    </>
  );
}

export default GoitReact;

const Wrapper = styled.div`
  width: fit-content;
  margin: 0 auto;
  background-color: #d7e3e3;
  background-image: radial-gradient(
      at 47% 33%,
      hsl(180, 6%, 94%) 0,
      transparent 59%
    ),
    radial-gradient(at 82% 65%, hsl(0, 39%, 77%) 0, transparent 55%);
`;

const Title = styled.h2`
  padding: 0 10px;
  width: fit-content;
  background: rgba(255, 255, 255, 0.2);
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid rgba(255, 255, 255, 0.3);
`;
