import "../styles/globals.css";
import Container from "../components/layouts";
import Navbar from "../components/navbar";

export default function App({ Component, pageProps }) {
  return (
    <>
      <Navbar />
      <Container>
        <Component {...pageProps} />
      </Container>
    </>
  );
}
