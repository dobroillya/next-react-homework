import { useState } from "react";
import ContactForm from "../../components/goit/contacts/ContactForm/ContactForm";
import ContactList from "../../components/goit/contacts/ContactList/ContactList";
import FilterInput from "../../components/goit/contacts/FilterInput/FilterInput";
import { nanoid } from "nanoid";

const initialValue = {
  name: "",
  phone: "",
  id: "",
};

function ContactsBook() {
  const [contacts, setContacts] = useState([]);
  const [search, setSearch] = useState("");
  const [input, setInput] = useState(initialValue);

  function handleSearch(e) {
    const searchValue = e.target.value;
    setSearch(searchValue);
  }

  function handleSubmit(e) {
    e.preventDefault();

    if (contacts.map((el) => el.name).includes(input.name)) {
      alert(`${input.name} is already in contacts`);
      setInput(initialValue);
    } else {
      const add = [...contacts, { ...input, id: nanoid() }];
      setInput(initialValue);
      setContacts(add);
    }
  }

  function handleDelete(id) {
    console.log(id);
    const del = contacts.filter((el) => el.id !== id);
    setContacts(del);
  }

  return (
    <>
      <ContactForm
        input={input}
        setInput={setInput}
        handleSubmit={handleSubmit}
      />

      <h2 className="text-left text-xl my-5">Contacts:</h2>

      <FilterInput handleSearch={handleSearch} search={search} />

      <ContactList
        contacts={contacts}
        search={search}
        handleDelete={handleDelete}
      />
    </>
  );
}

export default ContactsBook;
