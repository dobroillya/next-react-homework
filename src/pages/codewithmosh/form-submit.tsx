import ExpenseList from "../../components/mosh/expense-tracker/ExpenseList";
import ExpenseFilter from "../../components/mosh/expense-tracker/ExpenseFilter";
import ExpenseForm from "../../components/mosh/expense-tracker/ExpenseForm";
import { useState } from "react";

const expensesData = []

function ExpenceFormExercise() {
  const [selectedCategory, setselectedCategory] = useState('')
  const [expences, setExpences] = useState(expensesData);

  const visibleExpenses = selectedCategory
    ? expences.filter(el => el.category === selectedCategory)
    : expences

  return (
  <>
    <ExpenseForm onSubmit={(data) => setExpences([...expences, {...data, id: expences.length + 1 }])} />
    <ExpenseFilter onSelectCategory = {(category) => setselectedCategory(category)}/>
    <ExpenseList expenses={visibleExpenses} onDelete={(idDel) => setExpences(expences.filter(el => el.id !== idDel))}/>
  </>

  );
}

export default ExpenceFormExercise;
