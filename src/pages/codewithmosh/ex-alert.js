import { useState } from "react";
import Button from "../../components/mosh/Button";
import Alert from "../../components/mosh/Alert";

function ExAlert() {
  const [state, setState] = useState(false);

  function handleClick() {
    console.log(111);
    setState(true)
  }

  function handleClose(){
    setState(false)
  }

  return (
    <div className="text-center">
      <h2 >Showing an Alert</h2>
      <p className="text-lg text-center pb-8 ">with Tailwind css</p>
      <Button
        onClick={handleClick}
        type='primary'
        text="Click me"
      />

      {state && <Alert handleClose={handleClose}/>}
    </div>
  );
}

export default ExAlert;
