import Button from "../../components/mosh/Button";

function ExButton(buttons) {


    return (<>
        <h2 className="text-center">Building a Button Component</h2>
        <p className="text-lg text-center pb-8 ">with Tailwind css</p>
        <div className="flex flex-col justify-center items-center gap-2">
            <p className="pt-2">color: bg-indigo-600</p> <Button type={'primary'} >Text</Button>
            <p className="pt-2">color: bg-red-600</p><Button type={'alert'} >Text </Button>
            <p className="pt-2">color: bg-green-600</p><Button type={'ok'} >Text</Button>
        </div>
        </>

    );
}

export default ExButton;