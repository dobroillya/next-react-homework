import Button from "../../mosh/Button";
import styled from "styled-components";

function FeedbackOptions({options, onLeaveFeedback}) {
    return (
        <WrapperButtons>
        <Button
          onClick={() => {
            let updatedValue = options["good"] + 1;
            onLeaveFeedback({ ...options, good: updatedValue });
          }}
          type={'white'}
          py="py-1"
          text="Good"
          color="text-zinc-500"
        />
        <Button
        onClick={() => {
            let updatedValue = options["neutral"] + 1;
            onLeaveFeedback({ ...options, neutral: updatedValue });
          }}
          type={'white'}
          py="py-1"
          text="Neutral"
          color="text-zinc-500"
        />
        <Button
        onClick={() => {
            let updatedValue = options["bad"] + 1;
            onLeaveFeedback({ ...options, bad: updatedValue });
          }}
        type={'white'} py="py-1" text="Bad" color="text-zinc-500" />
      </WrapperButtons>
    );
}

export default FeedbackOptions;


const WrapperButtons = styled.div`
  display: flex;
  gap: 10px;
  padding-top: 30px;
  padding-bottom: 30px;
`;