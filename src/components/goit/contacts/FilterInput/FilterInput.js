import React from "react";

function FilterInput({search, handleSearch}) {
  return (
    <>
      <label
        className="block text-sm font-medium leading-6 text-gray-900"
        htmlFor="input-search"
      >
        Find contacts by name
      </label>
      <input
        onChange={(event) => {
          handleSearch(event);
        }}
        value={search}
        id="input-search"
        className="block rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-2 py-1 "
        type="text"
      />
    </>
  );
}

export default FilterInput;
