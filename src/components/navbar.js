import { Disclosure, Menu, Transition } from "@headlessui/react";
import {
  Bars3Icon,
  BuildingLibraryIcon,
  XMarkIcon,
  ChevronDownIcon,
} from "@heroicons/react/24/outline";
import { Fragment } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuLink from "./menuLink";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const links = [
  { name: "Dan-it Classwork", href: "/dan-it-classwork", id: "1" },
  { name: "Dan-it Homework", href: "/dan-it-homework", id: "2" },
  { name: "CodeWithMosh React", href: "/codewithmosh", id: "3" },
  { name: "GOIT React", href: "/go-it-react", id: "4" },
];

function Navbar() {
  const router = useRouter();
  const currentRoute = router.pathname;
  return (
    <Disclosure as="nav" className="bg-white shadow">
      {({ open }) => (
        <>
          <div className="flex justify-between mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            <div className="flex h-16 gap-6">
              <div className="flex flex-shrink-0 items-center">
                <Link href="/">
                  <BuildingLibraryIcon
                    className={
                      currentRoute === "/"
                        ? "h-10 w-10 stroke-indigo-500 hover:stroke-indigo-400"
                        : "h-10 w-10 stroke-indigo-900 hover:stroke-indigo-500"
                    }
                  />
                </Link>
              </div>
              <Menu
                as="div"
                className="relative hidden sm:ml-6 sm:flex sm:space-x-8"
              >
                {/* Current: "border-indigo-500 text-gray-900", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" */}

                {links.map((el) => {
                  return (
                    <MenuLink
                      key={el.id}
                      name={el.name}
                      href={el.href}
                      currentRoute={currentRoute}
                    />
                  );
                })}
              </Menu>
            </div>

            <div className="-mr-2 flex items-center sm:hidden">
              {/* Mobile menu button */}
              <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Open main menu</span>
                {open ? (
                  <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                ) : (
                  <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                )}
              </Disclosure.Button>
            </div>
          </div>

          <Disclosure.Panel className="sm:hidden">
            <div className="space-y-1 pb-3 pt-2">
              {links.map((el) => {
                return (
                  <Disclosure.Button
                    key={el.id}
                    as="a"
                    href={el.href}
                    className={currentRoute === el.href
                      ? 'block border-l-4 border-indigo-500 bg-indigo-50 py-2 pl-3 pr-4 text-base font-medium text-indigo-700'
                      : 'block px-4 py-2 text-base font-medium text-gray-500 hover:bg-gray-100 hover:text-gray-800'}
                  >
                    {el.name}
                  </Disclosure.Button>
                );
              })}

            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}

export default Navbar;
