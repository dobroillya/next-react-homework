import styled from "styled-components";


function Statistics({good, neutral, bad, total, positivePercentage}) {
    return (
    <WrapperList>
        <li>Good: {good}</li>
        <li>Neutral: {neutral}</li>
        <li>Bad: {bad}</li>
        <li>Tolal: {total}</li>
        <li>Positiv feedback: {positivePercentage} </li>
      </WrapperList>

    );
}

export default Statistics;




const WrapperList = styled.ul`
  width: fit-content;
  padding-top: 20px;

  li {
    font-size: 1.2rem;
    font-weight: 500;
  }
  `