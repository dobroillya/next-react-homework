import Link from "next/link";

function MenuLink({ currentRoute, name, href }) {
  return (
    <>
      <Link
        href={href}
        className={currentRoute === href
                    ? "inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900"
                    : "inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700"}
      >
        {name}
      </Link>
    </>
  );
}

export default MenuLink;
