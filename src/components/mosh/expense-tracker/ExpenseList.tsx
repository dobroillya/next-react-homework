import React from "react";

interface Expense {
  id: number;
  description: string;
  amount: number;
  category: string;
}

interface Props {
  expenses: Expense[];
  onDelete: (id: number) => void;
}

const ExpenseList = ({ expenses, onDelete }: Props) => {
  if (expenses.length === 0) return null;

  return (
    <table className="min-w-full divide-y divide-gray-300">
      <thead>
        <tr>
          <th className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">Description</th>
          <th className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Amount</th>
          <th className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Category</th>
          <th></th>
        </tr>
      </thead>
      <tbody className="divide-y divide-gray-200">
        {expenses.map((expense) => (
          <tr key={expense.id}>
            <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0">{expense.description}</td>
            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">${expense.amount.toFixed(2)}</td>
            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{expense.category}</td>
            <td>
              <button
                className="text-indigo-600 hover:text-indigo-900"
                onClick={() => onDelete(expense.id)}
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td>Total</td>
          <td className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">${expenses.reduce((acc, expense) => expense.amount + acc, 0).toFixed(2)}</td>
          <td></td>
          <td></td>
        </tr>
      </tfoot>
    </table>
  );
};

export default ExpenseList;