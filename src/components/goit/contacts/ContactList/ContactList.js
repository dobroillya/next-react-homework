import React from "react";
import Button from "../../../../components/mosh/Button";

function ContactList({ contacts, search, handleDelete }) {
  return (
    <ul role="list" className="grid grid-cols-1 gap-2 w-96 mt-5">
      {contacts
        .filter((person) => {
          if (search.length > 0) {
            return person.name.toLowerCase().includes(search.toLowerCase());
          } else {
            return person;
          }
        })
        .map((el) => (
          <li
            key={el.id}
            className="ml-5 py-1 px-5 col-span-1 flex flex-row justify-between items-center divide-y divide-gray-400 bg-white shadow"
          >
            {el.name} {": "} {el.phone}
            <Button type={"primary"} onClick={() => handleDelete(el.id)}>
              Delete
            </Button>
          </li>
        ))}
    </ul>
  );
}

export default ContactList;
