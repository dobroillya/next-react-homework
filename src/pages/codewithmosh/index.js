import Head from 'next/head';
import LinksHomework from '../../components/linksHomework';

const exercises = [
  {
    id: 'ex1',
    name: 'Building a Button Component',
    hrefGitLab: '',
    href: 'codewithmosh/ex-button',
    level: 'starter',
  },
  {
    id: 'ex2',
    name: 'Showing an Alert',
    hrefGitLab: '',
    href: 'codewithmosh/ex-alert',
    level: 'starter',
  },
  {
    id: 'ex3',
    name: 'Project Expense Tracker',
    hrefGitLab: '',
    href: 'codewithmosh/form-submit',
    level: 'beginer',
  },
  {
    id: 'ex4',
    name: 'Video game discovery app',
    hrefGitLab: '',
    href: 'codewithmosh/discovery-app',
    level: 'beginer',
  },
];

export default function CodeMosh() {
  return (
    <>
      <Head>
        <title>Code with Mosh</title>
      </Head>
      <h2 className="font-bold text-2xl text-zinc-700 text-center underline decoration-indigo-100 decoration-wavy decoration-2">
        Code with Mosh - React course exercises
      </h2>
      <LinksHomework list={exercises} />
    </>
  );
}
