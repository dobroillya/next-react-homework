import React from "react";

function ContactForm({ input, setInput, handleSubmit }) {
  return (
    <>
    <form onSubmit={handleSubmit}>
      <label
        className="block text-sm font-medium leading-6 text-gray-900"
        htmlFor="input-contact"
      >
        Name:
      </label>
      <input
        onChange={(event) => {
          setInput({ ...input, name: event.target.value });
        }}
        value={input.name}
        id="input-contact"
        className="block rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-2 py-1 "
        type="text"
        name="name"
        pattern="^[a-zA-Zа-яА-Я]+(([' -][a-zA-Zа-яА-Я ])?[a-zA-Zа-яА-Я]*)*$"
        title="Name may contain only letters, apostrophe, dash and spaces. For example Adrian, Jacob Mercer, Charles de Batz de Castelmore d'Artagnan"
        required
      />
      <label
        className="mt-2 block text-sm font-medium leading-6 text-gray-900"
        htmlFor="input-number"
      >
        Phone:
      </label>
      <input
        onChange={(event) => {
          setInput({ ...input, phone: event.target.value });
        }}
        id="input-number"
        value={input.phone}
        type="tel"
        name="number"
        className="block rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-2 py-1 "
        pattern="\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}"
        title="Phone number must be digits and can contain spaces, dashes, parentheses and can start with +"
        required
      />

      <div className="mt-5">
        <button
          type="submit"
          className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-500"
        >
          Add contact
        </button>
      </div>
      </form>
    </>
  );
}

export default ContactForm;
