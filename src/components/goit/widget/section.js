import styled from "styled-components";

function Section({children, title}) {
    return (  <>
    <Title>{title}</Title>
    {children}
    </>);
}

export default Section;

const Title = styled.h2`
  font-weight: 500;
  text-align: left;
`;