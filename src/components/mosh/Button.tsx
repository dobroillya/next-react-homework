import { FormEvent } from 'react'


export const btn = {
  primary: 'transition inline-flex items-center justify-center font-medium leading-6 text-white whitespace-no-wrap px-4 py-1 bg-indigo-600 border border-gray-200 rounded-md shadow-sm hover:bg-indigo-500',
  alert: 'transition inline-flex items-center justify-center font-medium leading-6 text-white whitespace-no-wrap px-4 py-1 bg-red-600 border border-gray-200 rounded-md shadow-sm hover:bg-red-500',
  ok: 'transition inline-flex items-center justify-center font-medium leading-6 text-white whitespace-no-wrap px-4 py-1 bg-green-600 border border-gray-200 rounded-md shadow-sm hover:bg-green-500',
  white: 'transition inline-flex items-center justify-center text-base font-medium leading-6 text-gray-600 whitespace-no-wrap px-4 py-1 bg-white border border-gray-200 rounded-md shadow-sm hover:bg-gray-50'
}

interface Props {
  children: string;
  type: string;
  text: string;
  onClick: (e: FormEvent) => void;
}

function Button({type, onClick, children, text }: Props) {

  return (
      <button
        onClick={onClick}
        type="button"
        className={`${btn[type]}`}
      >
       {children}{text}
      </button>

  );
}

export default Button;
