import Link from "next/link";

function LinksHomework({ list }) {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 pt-8 gap-4">
      {list.map((el) => {
        return (
          <div className="p-2 shadow-sm text-zinc-600" key={el.id}>
            <h3 className="text-lg  font-bold ">
              <Link href={el.href}>{el.name}</Link>
            </h3>
            <p>
              <b>Level: </b>
              {el.level}
            </p>
            <p className="pt-4">
              <Link href={el.hrefGitLab}>Gitlab repository</Link>
            </p>
          </div>
        );
      })}
    </div>
  );
}

export default LinksHomework;
