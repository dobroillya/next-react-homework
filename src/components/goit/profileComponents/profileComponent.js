import Image from "next/image";

function ProfileComponent({ username, tag, location, avatar, stats }) {
  return (
    <div className="profile mx-auto w-80 mt-10 flex flex-col gap-5 justify-center items-center shadow-md">
      <div className="description flex flex-col gap-2 items-center">
        <img
          width="200"
          height="300"
          src={avatar}
          alt="User avatar"
          className="avatar"
        />
        <p className="name text-2xl font-bold">{username}</p>
        <p className="tag text-zinc-600">{tag}</p>
        <p className="location text-zinc-600">{location}</p>
      </div>

      <ul className="w-full stats flex  justify-evenly  bg-slate-100  border-t">
        <li className="flex flex-col gap-1 items-center flex-1 border-r px-1 py-2">
          <span className="label text-zinc-400">Followers</span>
          <span className="quantity font-bold">{stats.followers}</span>
        </li>
        <li className="flex flex-col gap-1 items-center flex-1 border-r px-1 py-2">
          <span className="label text-zinc-400">Views</span>
          <span className="quantity font-bold">{stats.views}</span>
        </li>
        <li className="flex flex-col gap-1 items-center flex-1 px-1 py-2">
          <span className="label text-zinc-400">Likes</span>
          <span className="quantity font-bold">{stats.likes}</span>
        </li>
      </ul>
    </div>
  );
}

export default ProfileComponent;
