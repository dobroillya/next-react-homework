import styled from "styled-components";

function FriendListItem({ avatar, name, isOnline}) {
  return (
    <Wrapper>
      <Item className="item">
        <Online className="status" inputColor={isOnline}></Online>
        <img className="avatar" src={avatar} alt="User avatar" width="48" />
        <p className="name">{name}</p>
      </Item >
    </Wrapper>
  );
}

export default FriendListItem;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
    margin-top: 20px;
    padding: 10px 10px 10px 20px;
    width: 200px;
    margin-left: auto;
    margin-right: auto;
    background-color: beige;
`

const Item = styled.li`
    display: flex;
    align-items: center;
    gap: 10px;

`

const Online = styled.span`
        width: 15px;
        height: 15px;
        margin-right: 15px;
        border-radius: 50%;
        background-color: ${props => props.inputColor ? 'green' : 'red'};
`
