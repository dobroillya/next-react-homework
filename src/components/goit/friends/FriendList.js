import FriendListItem from "./FriendListItem";

function FriendList({ friends }) {
  return (
    <>
      {friends.map((el) => {
        return (
          <FriendListItem
            key={el.id}
            avatar={el.avatar}
            name={el.name}
            isOnline={el.isOnline}
          />
        );
      })}
    </>
  );
}

export default FriendList;
