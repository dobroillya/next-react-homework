import ProfileComponent from "../../components/goit/profileComponents/profileComponent";

const user = {
  username: "Jacques Gluke",
  tag: "jgluke",
  location: "Ocho Rios, Jamaica",
  avatar: "https://cdn-icons-png.flaticon.com/512/2922/2922506.png",
  stats: {
    followers: 5603,
    views: 4827,
    likes: 1308,
  },
};

function Profile() {
  return (
    <>
      <h2>Профіль соціальної мережі</h2>
      <ProfileComponent
        username={user.username}
        tag={user.tag}
        location={user.location}
        avatar={user.avatar}
        stats={user.stats}
      />
    </>
  );
}

export default Profile;
